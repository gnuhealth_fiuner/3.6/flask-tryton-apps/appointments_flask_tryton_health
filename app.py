import os

from flask import Flask, flash, render_template, url_for, request, redirect, session
from flask_tryton import Tryton
from flask_babel import Babel
from flask_wtf import FlaskForm
from datetime import datetime

from functools import wraps

from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms.fields import PasswordField

app = Flask(__name__)
babel = Babel(app)
app.config['TRYTON_DATABASE'] = os.environ.get('DNAME', 'DNAME')
app.config['TRYTON_CONFIG'] = 'trytond.conf_url'

tryton = Tryton(app, configure_jinja=True)

WebUser = tryton.pool.get('web.user')
Session = tryton.pool.get('web.user.session')
Appointments = tryton.pool.get('gnuhealth.appointment')

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        session_key = None
        if 'session_key' in session:
            session_key = session['session_key']
        user = Session.get_user(session_key)
        if not user:
            return redirect(url_for('login', next=request.path))
        return func(*args, **kwargs)
    return wrapper


class LoginForm(FlaskForm):                                                    
    email = StringField('Correo Electrónico',                                  
        validators=[DataRequired()],                                           
        render_kw={'placeholder': 'Correo Electrónico'})                       
    password = PasswordField('Contraseña',                                     
        validators=[DataRequired()],                                           
        render_kw={'placeholder': 'Contraseña'})                               


@app.route('/', methods=['GET', 'POST'])                                  
@tryton.transaction()
def index():
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        try:
            user = WebUser.authenticate(form.email.data, form.password.data)
            if user:
                session['session_key'] = WebUser.new_session(user)
                flash('Ahora estas identificado.', 'success')
                return redirect(request.form.get('next', url_for('appointments')))
            flash('Email o contraseña incorrectos.', 'error')
        except:
            return 'Demasiados intentos de ingreso. Espere un momento y vuelvalo a intentar'
    return render_template(
        'index.html',
        form=form,
        next=request.args.get('next'))

@app.route('/logout')
@tryton.transaction(readonly=False)
@login_required
def logout():
    if session['session_key']:
        Session.delete(Session.search(
                ('key', '=', session['session_key']),
                ))
        session.pop('session_key', None)
        flash("Se ha desconectado tu sesión", 'success')                      
    return redirect(url_for('index'))

@app.route('/appointments', methods=['GET','POST'])
@tryton.transaction()
@login_required
def appointments():
    today_start = datetime.today().replace(hour=0,minute=0,second=0)
    today_end = datetime.today().replace(hour=23,minute=59,second=59)
    appointments = Appointments.search([('appointment_date','>',today_start),
                                        ('appointment_date','<',today_end),
                                        ('state','=','confirmed')])
    
    today = datetime.today().date().strftime('%d %m %Y')
    return render_template('appointments.html', 
                           appointments=appointments,
                           today=today)


if __name__ == "__main__":
    app.run(debug=True)
